package com.example.uapv1703110.edtuapv;

public enum Type {
    CM,
    TD,
    TP,
    EVAL,
    UNDEFINED;
}
