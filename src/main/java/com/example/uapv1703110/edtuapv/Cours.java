package com.example.uapv1703110.edtuapv;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

public class Cours {
    private Date dateDebut;
    private Date dateFin;
    private String ue;
    private String salle;
    private String enseignant;
    private Type type;
    private String promotion;
    private String groupes;

    public Date getDateDebut() {
        return dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public String getUe() {
        return ue;
    }

    public String getSalle() {
        return salle;
    }

    public String getEnseignant() {
        return enseignant;
    }

    public Type getType() {
        return type;
    }

    public String getPromotion() {
        return promotion;
    }

    public String getGroupes() {
        return groupes;
    }

    public Cours(Date dateDebut, Date dateFin, String ue, String salle, String enseignant, Type type, String promotion, String groupes) {
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.ue = ue;
        this.salle = salle;
        this.enseignant = enseignant;
        this.type = type;
        this.promotion = promotion;
        this.groupes = groupes;
    }

    public Cours(Map<String, String> map) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("CEST"));
        try {
            Date dDebut = sdf.parse(map.get("DTSTART"));
            Date dEnd = sdf.parse(map.get("DTEND"));
            this.dateDebut = dDebut;
            this.dateFin = dEnd;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Map <String,String> desc = getDesc(map.get("DESCRIPTION"));
        this.ue = desc.get("Matière");

        if (desc.containsKey("Salle")) {
            this.salle = desc.get("Salle");
            this.salle = this.salle.replace("\\","");
        }

        if (!desc.containsKey("Type")) {
            this.type = Type.UNDEFINED;
        }
        else {
            String type = desc.get("Type");
            switch (type) {
                case "Evaluation":
                    this.type = Type.EVAL;
                    break;
                case "CM":
                    this.type = Type.CM;
                    break;
                case "TD":
                    this.type = Type.TD;
                    break;
                case "TP":
                    this.type = Type.TP;
                    break;
                default:
                    this.type = Type.UNDEFINED;
            }
        }

        if (this.type != Type.UNDEFINED) {
            if (desc.containsKey("Enseignant")) this.enseignant = desc.get("Enseignant");
            else {
                String ens = desc.get("Enseignants");
                ens = ens.replace("\\", "");
                this.enseignant = ens;
            }
        }
        if (this.type == Type.CM) {
            if (desc.containsKey("Promotion")) {
                this.promotion = desc.get("Promotion");
                this.groupes = null;
            }
            else if (desc.containsKey("Promotions")) {
                String promo = desc.get("Promotions");
                promo = promo.replace("\\", "");
                this.promotion = promo;
                this.groupes = null;
            }
            else {
                String grp = desc.get("TD");
                grp = grp.replace("\\", "");
                this.groupes = grp;
                this.promotion = null;
            }

        }

        if (this.type == Type.TD || this.type == Type.TP) {
            String grp = desc.get("TD");
            grp = grp.replace("\\", "");
            this.groupes = grp;
            this.promotion = null;
        }

        else if (this.type == Type.EVAL) {
            if (desc.containsKey("Promotion")) this.promotion = desc.get("Promotion");
            else if (desc.containsKey("Promotions")) {
                String promo = desc.get("Promotions");
                promo = promo.replace("\\", "");
                this.promotion = promo;
                this.groupes = null;
            }
            else if (desc.containsKey("TD")) {
                String grp = desc.get("TD");
                grp = grp.replace("\\", "");
                this.groupes = grp;
                this.promotion = null;
            }
        }

    }

    public Map<String,String> getDesc(String s) {
        Map <String,String> desc = new HashMap<String, String>();
        s = s.replace("LANGUAGE=fr:","");
        s= s.replace(" : ", ":");
        s= s.replace("\\n", "<X>");
        String[] pairs = s.split("<X>");
        for (int i=0;i<pairs.length;i++) {
            String pair = pairs[i];
            String[] keyValue = pair.split(":");
            desc.put(keyValue[0], keyValue[1]);
        }
        return desc;
    }

    @Override
    public String toString() {
        return "Cours{" +
                "dateDebut=" + dateDebut +
                ", dateFin=" + dateFin +
                ", ue='" + ue + '\'' +
                ", salle='" + salle + '\'' +
                ", enseignant='" + enseignant + '\'' +
                ", type=" + type +
                ", promotion='" + promotion + '\'' +
                ", groupes='" + groupes + '\'' +
                '}';
    }
}
