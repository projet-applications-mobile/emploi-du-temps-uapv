package com.example.uapv1703110.edtuapv;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CalendarView;

public class Calendrier extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendrier);
        CalendarView view = (CalendarView)findViewById(R.id.calendarView);

        view.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView arg0, int year, int month,
                                            int date) {
                Intent intent = new Intent(Calendrier.this, Jour.class);
                intent.putExtra("year",year);
                intent.putExtra("month",month);
                intent.putExtra("date",date);
                startActivity(intent);
            }
        });
    }
}
