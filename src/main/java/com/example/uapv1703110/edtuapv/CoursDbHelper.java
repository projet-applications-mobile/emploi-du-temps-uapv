package com.example.uapv1703110.edtuapv;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class CoursDbHelper extends SQLiteOpenHelper {

    private static final String TAG = CoursDbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "cours.db";

    public static final String TABLE_NAME = "cours";

    public static final String _ID = "_id";
    public static final String COLUMN_MATIERE = "matiere";
    public static final String COLUMN_ENSEIGNANT = "enseignant";
    public static final String COLUMN_PROMOTION = "promotion";
    public static final String COLUMN_SALLE = "salle";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_GROUPE = "groupe";
    public static final String COLUMN_DTSTART = "dtstart";
    public static final String COLUMN_DTEND = "dtend";
    private int annee;
    private int groupe;

    public CoursDbHelper(Context context,int annee,int groupe) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.annee = annee;
        this.groupe = groupe;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_COURS_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                COLUMN_MATIERE + " TEXT NOT NULL, " +
                COLUMN_ENSEIGNANT + " TEXT, " +
                COLUMN_PROMOTION + " INTEGER, " +
                COLUMN_SALLE+ " TEXT, " +
                COLUMN_TYPE+ " TEXT, " +
                COLUMN_GROUPE+ " TEXT, " +
                COLUMN_DTSTART+ " LONG, " +
                COLUMN_DTEND+ " LONG) ";

        db.execSQL(SQL_CREATE_COURS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void addCours(Cours cours) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_MATIERE, cours.getUe());
        values.put(COLUMN_ENSEIGNANT, cours.getEnseignant());
        values.put(COLUMN_PROMOTION, cours.getPromotion());
        values.put(COLUMN_SALLE, cours.getSalle());
        values.put(COLUMN_TYPE, cours.getType().toString());
        values.put(COLUMN_GROUPE, cours.getGroupes());
        values.put(COLUMN_DTSTART, cours.getDateDebut().getTime());
        values.put(COLUMN_DTEND, cours.getDateFin().getTime());
        db.insert(TABLE_NAME,null,values);
        db.close();
    }

    public ArrayList<Cours> fetchAllCoursForDate(Date calendarDate) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from "+ TABLE_NAME,null);
        ArrayList<Cours> coursDuJour = new ArrayList<Cours>();
        if (cursor != null) {
            cursor.moveToFirst();
        }
        do {
            Date d = new Date(cursor.getLong(7));
            if (d.getDate() == calendarDate.getDate() && d.getMonth() == calendarDate.getMonth() && d.getYear() == calendarDate.getYear()) {
                coursDuJour.add(cursorToCours(cursor));
            }
        } while (cursor.moveToNext());

        return coursDuJour;
    }



    public ArrayList<Cours> fetchAllCoursForGroup() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from "+ TABLE_NAME + " where " + COLUMN_GROUPE +" LIKE '%L" +annee+"INFO_TD"+ groupe+"%' or "+ COLUMN_PROMOTION+" LIKE '%L" +annee+" INFORMATIQUE%'",null);
        ArrayList<Cours> coursDuJour = new ArrayList<Cours>();
        if (cursor != null) {
            cursor.moveToFirst();
        }
        do {
            coursDuJour.add(cursorToCours(cursor));
        } while (cursor.moveToNext());

        return coursDuJour;
    }

    public ArrayList<Cours> fetchAllCoursForGroupAndDate(String grp,Date calendarDate) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from "+ TABLE_NAME + " where " + COLUMN_GROUPE +" LIKE '%L"+annee+"INFO_TD"+ groupe+"% or "+ COLUMN_PROMOTION+" LIKE '%L3 INFORMATIQUE%'",null);
        ArrayList<Cours> coursDuJour = new ArrayList<Cours>();
        if (cursor != null) {
            cursor.moveToFirst();
        }
        do {
            Date d = new Date(cursor.getLong(7));
            if (d.getDate() == calendarDate.getDate() && d.getMonth() == calendarDate.getMonth() && d.getYear() == calendarDate.getYear()) {
                coursDuJour.add(cursorToCours(cursor));
            }
        } while (cursor.moveToNext());

        ArrayList<Date> datesdebuts = new ArrayList<Date>();
        ArrayList<Cours> coursfinaux = new ArrayList<Cours>();
        for (Cours c : coursDuJour) {
            boolean deja = false;
            for (Date d : datesdebuts) {
                if (c.getDateDebut().getTime() == d.getTime()) deja = true;

            }
            if (deja == false) coursfinaux.add(c);
            datesdebuts.add(c.getDateDebut());
        }
        return coursfinaux;
    }

    public Cours getNextCours() {
        ArrayList<Cours> cours = fetchAllCoursForGroup();
        Date curr = new Date();
        long min_diff = -1;
        Cours following = null;
        for (Cours c : cours) {
            if (c.getDateDebut().after(curr) && (min_diff == -1 || c.getDateDebut().getTime()-curr.getTime() < min_diff)) {
                following = c;
                min_diff = c.getDateDebut().getTime()-curr.getTime();
            }
        }
        return following;
    }

    public Cours getSecondToNextCours() {
        ArrayList<Cours> cours = fetchAllCoursForGroup();
        Date curr = new Date();
        long min_diff = -1;
        Cours following = null;
        for (Cours c : cours) {
            if (c.getDateDebut().after(curr) && (min_diff == -1 || c.getDateDebut().getTime()-curr.getTime() < min_diff)) {
                following = c;
                min_diff = c.getDateDebut().getTime()-curr.getTime();
            }
        }
        curr = following.getDateDebut();
        min_diff = -1;
        for (Cours c : cours) {
            if (c != following && c.getDateDebut().after(curr) && (min_diff == -1 || c.getDateDebut().getTime()-curr.getTime() < min_diff)) {
                following = c;
                min_diff = c.getDateDebut().getTime()-curr.getTime();
            }
        }
        return following;
    }
    public Cours cursorToCours(Cursor cursor) {
        Cours cours = new Cours(
                new Date(cursor.getLong(cursor.getColumnIndex(COLUMN_DTSTART))),
                new Date(cursor.getLong(cursor.getColumnIndex(COLUMN_DTEND))),
                cursor.getString(cursor.getColumnIndex(COLUMN_MATIERE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_SALLE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ENSEIGNANT)),
                Type.valueOf(cursor.getString(cursor.getColumnIndex(COLUMN_TYPE))),
                cursor.getString(cursor.getColumnIndex(COLUMN_PROMOTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_GROUPE))
        );
        return cours;
    }

}
