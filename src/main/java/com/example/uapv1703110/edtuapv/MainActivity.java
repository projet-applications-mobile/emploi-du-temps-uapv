package com.example.uapv1703110.edtuapv;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.support.v7.app.AppCompatActivity;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FetchCoursTask ct = new FetchCoursTask(MainActivity.this);
        ct.execute();

    }

    class FetchCoursTask extends AsyncTask<URL, Integer, Long> {

        private Context mContext;

        public FetchCoursTask(Context context) {
            mContext = context;
        }

        @Override
        protected Long doInBackground(URL... urls) {
            try {
                CoursDbHelper cdh = new CoursDbHelper(mContext,3,3);
                ArrayList<Cours> a = ICalParser.ParseUrl("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN", cdh);
                System.out.println("Testetestes");
                Log.e("Next:", cdh.getNextCours().toString());
                Log.e("Second to Next:", cdh.getSecondToNextCours().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long l) {
            Intent intent = new Intent(mContext, DailyActivity.class);
            startActivity(intent);
        }

    }
}
