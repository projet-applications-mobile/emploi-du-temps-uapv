package com.example.uapv1703110.edtuapv;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ICalParser {

    public static ArrayList<Cours> ParseUrl(String urlstring,CoursDbHelper cdh) throws IOException {
        ArrayList<Cours> edt = new ArrayList<Cours>();
        BufferedReader br = null;
        HttpURLConnection urlConnection = null;
        URL url = new URL(urlstring);
        br = new BufferedReader(new InputStreamReader(url.openStream()));
        try {
            String line = null;
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            while (!(line = br.readLine()).equals("END:VCALENDAR")) {
                br.readLine();
                br.readLine();
                line = br.readLine();
                if (!line.contains("UID")) line = br.readLine();
                if (line.contains("UID:Ferie")) {
                    br.readLine();
                    br.readLine();
                    br.readLine();
                    br.readLine();
                }
                else {
                    if (line.contains("UID:COURSANNULE")) {
                        line = br.readLine();
                        line = br.readLine();
                        line = br.readLine();
                        line = br.readLine();
                        if (!line.contains("DESCRIPTION")) line = br.readLine();
                        line = br.readLine();
                    } else {
                        Map<String, String> myMap = new HashMap<String, String>();
                        while (!(line = br.readLine()).equals("END:VEVENT")) {
                            String[] pairs;
                            if (line.indexOf(';') != -1) {
                                pairs = line.split(";");
                            } else {
                                pairs = line.split(":");
                            }
                            myMap.put(pairs[0], pairs[1]);
                        }
                        Cours c = new Cours(myMap);
                        cdh.addCours(c);
                        edt.add(c);
                    }

                }
            }
            return edt;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}


