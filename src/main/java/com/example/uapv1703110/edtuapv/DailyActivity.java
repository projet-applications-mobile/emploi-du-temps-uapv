package com.example.uapv1703110.edtuapv;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.net.URL;
import java.util.ArrayList;

public class DailyActivity extends AppCompatActivity {

    private TextView prochainCours,encoreProchainCours;
    private final String L1 = "L1";
    private final String L2 = "L2";
    private final String L3 = "L3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily);
        prochainCours = (TextView) findViewById(R.id.prochainCours);
        encoreProchainCours = (TextView) findViewById(R.id.encoreProchainCours);
        CoursDbHelper cdh = new CoursDbHelper(this,3,3);
        Cours c1 = cdh.getNextCours();
        Cours c2 = cdh.getSecondToNextCours();
        updateCours(c1,c2);

        prochainCours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(DailyActivity.this,Calendrier.class);
                startActivity(intent);
            }
        });
        encoreProchainCours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(DailyActivity.this,Calendrier.class);
                startActivity(intent);
            }
        });

    }

    public void updateCours(Cours c1, Cours c2) {
        this.prochainCours.setText("PROCHAIN COURS\nDe: "+c1.getDateDebut()+"\nà: "+c1.getDateFin()+"\n"+c1.getUe()+"\nEnseignants: "+c1.getEnseignant()+"\n"+c1.getSalle()+"\nType: "+c1.getType());
        if (c1.getType() == Type.EVAL) prochainCours.setBackgroundColor(Color.RED);
        else prochainCours.setBackgroundColor(Color.BLUE);
        this.encoreProchainCours.setText("COURS SUIVANT\nDe: "+c2.getDateDebut()+"\nà: "+c2.getDateFin()+"\n"+c2.getUe()+"\nEnseignants: "+c2.getEnseignant()+"\n"+c2.getSalle()+"\nType: "+c2.getType());
        if (c2.getType() == Type.EVAL) encoreProchainCours.setBackgroundColor(Color.RED);
        else encoreProchainCours.setBackgroundColor(Color.BLUE);
        prochainCours.setTextColor(Color.WHITE);
        encoreProchainCours.setTextColor(Color.WHITE);
    }

}

