package com.example.uapv1703110.edtuapv;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Jour extends AppCompatActivity {
    private static final String TAG = Jour.class.getSimpleName();
    private Calendar cal = Calendar.getInstance();
    private ImageView previousDay;
    private ImageView nextDay;
    private RelativeLayout mLayout;
    private int eventIndex;
    private TextView currentDate;
    private CoursDbHelper dbHelper;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jour);
        Intent intent = getIntent();
        cal.set(intent.getExtras().getInt("year"),intent.getExtras().getInt("month"),intent.getExtras().getInt("date"));
        dbHelper = new CoursDbHelper(this,3,3);
        mLayout = (RelativeLayout)findViewById(R.id.left_event_column);
        eventIndex = mLayout.getChildCount();
        currentDate = (TextView)findViewById(R.id.display_current_date);
        currentDate.setText(displayDateInString(cal.getTime()));
        displayDailyEvents();
        previousDay = (ImageView)findViewById(R.id.previous_day);
        nextDay = (ImageView)findViewById(R.id.next_day);
        previousDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousCalendarDate();
            }
        });
        nextDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextCalendarDate();
            }
        });
    }

    private void previousCalendarDate(){
        mLayout.removeViewAt(eventIndex - 1);
        cal.add(Calendar.DATE, -1);
        currentDate.setText(displayDateInString(cal.getTime()));
        displayDailyEvents();
    }
    private void nextCalendarDate(){
        mLayout.removeViewAt(eventIndex - 1);
        cal.add(Calendar.DATE, 1);
        currentDate.setText(displayDateInString(cal.getTime()));
        displayDailyEvents();
    }

    private String displayDateInString(Date mDate){
        SimpleDateFormat formatter = new SimpleDateFormat("d MMMM, yyyy", Locale.ENGLISH);
        return formatter.format(mDate);
    }

    private void displayDailyEvents(){
        Date calendarDate = cal.getTime();
        ArrayList<Cours> listeCours = dbHelper.fetchAllCoursForGroupAndDate("L3INFO_TD3",calendarDate);
        Log.e("ue",listeCours.toString());
        for(Cours cours : listeCours){
            int eventBlockHeight = getEventTimeFrame(cours.getDateDebut(), cours.getDateFin());
            Log.d(TAG, "Height " + eventBlockHeight);
            displayEventSection(cours.getDateDebut(), eventBlockHeight, cours);
        }
    }

    private int getEventTimeFrame(Date start, Date end){
        long timeDifference = end.getTime() - start.getTime();
        Calendar mCal = Calendar.getInstance();
        mCal.setTimeInMillis(timeDifference);
        int hours = mCal.get(Calendar.HOUR);
        int minutes = mCal.get(Calendar.MINUTE);
        return (hours * 60) + ((minutes * 60) / 100);
    }

    private void displayEventSection(Date eventDate, int height, Cours cours) {
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        String displayValue = timeFormatter.format(eventDate);
        String[] hourMinutes = displayValue.split(":");
        int hours = Integer.parseInt(hourMinutes[0]);
        int minutes = Integer.parseInt(hourMinutes[1]);
        Log.d(TAG, "Hour value " + hours);
        Log.d(TAG, "Minutes value " + minutes);
        int topViewMargin = (hours * 60) + ((minutes * 60) / 100);
        Log.d(TAG, "Margin top " + topViewMargin);
        hours = cours.getDateDebut().getHours() - 8;
        minutes = cours.getDateDebut().getMinutes() - 30;
        if (minutes < 0) {
            minutes = 30;
            hours = hours - 1;
        }
        topViewMargin = (hours - 1) * 90;
        if (cours.getDateFin().getHours() - cours.getDateDebut().getHours() == 1){
            height = 1;
        }
        else height = 2;
        createEventView(topViewMargin, height * 90, cours);
    }

    private void createEventView(int topMargin, int height, Cours cours){
        Log.e("createEventView: ", String.valueOf(topMargin));
        TextView mEventView = new TextView(Jour.this);
        RelativeLayout.LayoutParams lParam = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lParam.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        lParam.topMargin = topMargin * 2;
        lParam.leftMargin = 24;
        mEventView.setLayoutParams(lParam);
        mEventView.setPadding(24, 0, 24, 0);
        mEventView.setHeight(height * 2);
        mEventView.setGravity(0x11);
        mEventView.setTextColor(Color.parseColor("#ffffff"));
        mEventView.setText(cours.getUe()+"\n"+cours.getEnseignant());
        mEventView.setBackgroundColor(Color.parseColor("#3F51B5"));
        mLayout.addView(mEventView, eventIndex - 1);
    }
}
